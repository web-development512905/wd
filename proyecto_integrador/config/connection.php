<?php

    $host = 'localhost'; //127.0.0.1
    $dbname = 'siau';
    $dbuser = 'root'; //usuario de la base de datos
    $dbpassword = 'root';

    try{
        //PDO es un objeto
        $conn = new PDO("mysql:host=$host; dbname=$dbname", $dbuser, $dbpassword);

            //PDOException captura la excepcion o error que arroje y la manda a la variable $e
    } catch(PDOException $e) {
        echo "<H2> ERROR EN LA CONEXION DE LA BASE DE DATOS: ". $e->getMessage() . "</H2>";
    }
    
?>