<?php
        require 'connection.php';

                //usuario es un argumento si pusiera juan seria un parametro
        if(isset($_POST['your_email'], $_POST['your_pass'])){
            if(!empty($_POST['your_email'] && !empty($_POST['your_pass']))){

                $username = $_POST['your_email'];
                $password  = md5($_POST['your_pass']);
                //La conexion a mysql se suele hacer en otro archivo en este caso connection.php
                $query = "SELECT * FROM users WHERE email = '$username' AND password = '$password' ";
                
                $resultado = $conn->query($query);
                //Se imprime la cantidad de rows que retorno el query, si es cero pues no se encontró nada.
                //var_dump($resultado->rowCount());

                if($resultado->rowCount() > 0 ){
                    $registro = $resultado->fetch(PDO::FETCH_ASSOC);

                    session_start();
                    $_SESSION['user'] = $registro['name'];
                    $_SESSION['rol'] = $registro['role'];
                    $_SESSION['iduser'] = $registro['id'];
                    header("Location: dashboard/home.php");
                }else{
                    echo "<p style='color:#000; text-align:center; font-size:13px; position:absolute; bottom:23%; left:50%; transform:translateX(-50%); z-index:10;'> *Credenciales inválidas*</p>";
                }
            }else{
                echo "<p style='color:#000; text-align:center; font-size:13px; position:absolute; bottom:23%; left:50%; transform:translateX(-50%); z-index:10;'> *No puede haber campos vacíos*</p>";
            }
        }


        if(isset($_POST['my-email'])){
            if(!empty($_POST['my-email'])){
                $email = $_POST['my-email'];
                $query = "SELECT * FROM users WHERE email = '$email'";
                $resultado = $conn->query($query);
                if($resultado->rowCount()>0){
                    $registro = $resultado->fetch(PDO::FETCH_ASSOC);
                    $id = $registro['id'];
                    header("Location: reset-pass.php?id=$id");
                }else{
                    echo "<p style='color:#000; font-size:13px; position:absolute; bottom:30%; left:50%; transform:translateX(-50%); z-index:10;'> *Este correo no está registrado*</p>";
                }
            }else{
                echo "<p style='color:#000; font-size:13px; position:absolute; bottom:30%; left:50%; transform:translateX(-50%); z-index:10;'> *No puede haber campos vacíos*</p>";
            }
        }
        if(isset($_POST['my-pass'], $_POST['my-confirm-pass'])){
            if(!empty($_POST['my-pass']) && !empty($_POST['my-confirm-pass'])){
                if($_POST['my-pass'] === $_POST['my-confirm-pass']){
                    $id = $_GET['id'];
                    $new_pass = md5($_POST['my-pass']);
                    $sql = "UPDATE users SET password = :password WHERE id = :userId";
                    $stmt_update = $conn->prepare($sql);
                    $stmt_update->bindParam(':password', $new_pass);
                    $stmt_update->bindParam(':userId', $id);
                    $stmt_update->execute();
                    echo " <script type='text/javascript'> alert('Contraseña actualizada');</script>";
                    echo " <script type='text/javascript'> window.location.href = '../';</script>";
                }else{
                    echo "<p style='color:#000; font-size:13px; position:absolute; bottom:23%; left:50%; transform:translateX(-50%); z-index:10;'> *Las contraseñas no coinciden*</p>";
                }
            }else{
                echo "<p style='color:#000; font-size:13px; position:absolute; bottom:23%; left:50%; transform:translateX(-50%); z-index:10;'> *No puede haber campos vacíos*</p>";
            }
        }

?>