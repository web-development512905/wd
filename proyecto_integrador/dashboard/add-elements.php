<?php
    require "../mysql/functions.php";
    session_start();
    if(!isset($_SESSION["user"]) ||$_SESSION["rol"]!="admin"){
        echo '<script>window.history.go(-1)</script>';
        exit;
    }
    validate_user();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/add-elements.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title>Add user</title>
</head>
<body>
<?=include "../includes/header.php";?>
    <h2 class="h2-users">Register new person</h2>

    <form class="form-users" method="post">
        <input class="input-users" type="text" name="name" placeholder="Name *">
        <input class="input-users" type="hidden" name="last-name" placeholder="Last Name *">
        <input class="input-users" type="text" name="email" placeholder="Email Address *">
        <input class="input-users" type="password" name="pass" placeholder="Password *">
        <div class="custom-select">
            <select class="select" id="select" name="options">
                <option value="">Select a role *</option>
                <option value="admin">Admin</option>
                <option value="student">Student</option>
            </select>
        </div>
        <button class="button-users" type="submit">Register</button>
        <?php include "../mysql/registrar.php"?>
    </form>    
      
<?php 
    include "../includes/footer.php";
?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/eb29c0afa2.js" crossorigin="anonymous"></script>

</body>
</html>