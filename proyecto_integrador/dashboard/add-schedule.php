<?php 
    session_start();
    if(!isset($_SESSION["user"]) ||$_SESSION["rol"]!="student"){
        echo '<script>window.history.go(-1)</script>';
        exit;
    }
    require "../mysql/functions.php";
    if (isInSchedule($conn,$_SESSION['iduser'])== 1){
        echo "<p class='mt-2' style='text-align:center; color:red; font-style:italic;'>This subject is already registered in your schedule </p>";
    }else{
        addSubject($_SESSION['iduser'],$conn);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/subjects.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title>Add to Schedule</title>
</head>
<body>

        <div class="title-container">
            <h2 class="title">Student</h2>
            <div class="dropdown">
            <a class="btn btn-primary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Menu
            </a>

            <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="home.php">Home</a></li>
            <li><a class="dropdown-item" href="schedule.php">Schedule</a></li>
            <li><a class="dropdown-item" href="add-schedule.php">Add Subject to Schedule</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="tasklist.php">Task List</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="salir.php">Log out</a></li>
            </ul>
            </div>
        </div>
        <h1 class="text-center my-4 ">Add Subject to Schedule</h1>
        <form method="post">
            <label for="key-subject">Subject Key:</label>
            <input class="key-subject" type="text" name="key-subject">
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
        <div class="grid-header">
            <div class="title-header">Subject Key</div>
            <div class="title-header">Subject Name</div>
            <div class="title-header">Teacher Name</div>
        </div>

        <?php 
            record_subject2($conn);
            include "../includes/footer.php";
        ?>

    
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/eb29c0afa2.js" crossorigin="anonymous"></script>
</body>
</html>