<?php
    require "../mysql/functions.php";
    session_start();
    if(!isset($_SESSION["user"]) ||$_SESSION["rol"]!="admin"){
        echo '<script>window.history.go(-1)</script>';
        exit;
    }
    validate_user();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" cozntent="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/add-elements.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title>Adding subjects</title>
</head>
<body>
<?=include "../includes/header.php";?>

    <h2 class="h2-subjects">Add new subject</h2>
    <form class="form-subjects" method="post">
        <input class="input-subjects" type="text" name="key-subject" placeholder="Key Subject *">
        <input class="input-subjects" type="text" name="subject-name" placeholder="Subject Name *">
        <input class="input-subjects" type="text" name="teacher-name" placeholder="Teacher Name *">
        <button class="button-subjects" type="submit">Register</button>
        <?php include "../mysql/add-subject.php"?>
    </form>    
<?php 
    include "../includes/footer.php";
?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/eb29c0afa2.js" crossorigin="anonymous"></script>
</body>
</html>