<?php
    require "../mysql/functions.php";
    session_start();
    validate_user();
        if(isset($_GET['id']) && !empty($_GET['id'])){
            if($_SESSION['rol'] == "admin"){
                $usuario_id = $_GET['id'];
                $sql = "SELECT * FROM users WHERE id = :id";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(':id', $usuario_id);
                $stmt->execute();
            
                if($stmt->rowCount() > 0){
                    $usuario = $stmt->fetch(PDO::FETCH_ASSOC);
                }else{
                    echo '<script>window.history.go(-1)</script>';
                    exit;
                }
            }elseif($_SESSION['rol'] == "student"){
                $subject_id = $_GET['id'];
                $sql = "SELECT * FROM subjects WHERE id = :id";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(':id', $subject_id);
                $stmt->execute();
            
                if($stmt->rowCount() > 0){
                    $subject = $stmt->fetch(PDO::FETCH_ASSOC);
                }else{
                    echo '<script>window.history.go(-1)</script>';
                    exit;
                }
            }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/add-elements.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title><?=($_SESSION['rol']=="admin")?$usuario['name']:$subject['name']?></title>
</head>
<body>

<header>
    <?=include "../includes/header.php";?>
</header>

<?php 

    if(isset($_GET['mensaje']) && $_GET['mensaje'] === "actualizado"){
        echo "<h3 style='text-decoration:underline; font-style:italic;color:#fff; font-size: 1em; position:absolute; text-align:center; width:100%; top:5;'>*It's been edited successfully*</h3>";
    }    
    
    if($_SESSION['rol'] == "admin"){ ?>


    <h2 class="h2-users">Edit, <?= $usuario['name']?></h2>

    <form action="edit-elements.php?id=<?=$usuario['id']?>" class="form-users" method="post">
        <input class="input-users" type="text" name="name" placeholder="Name *" value="<?=$usuario['name']?>">
        <input class="input-users" type="hidden" name="last-name" placeholder="Last Name *">
        <input class="input-users" type="text" name="email" placeholder="Email Address *" value="<?=$usuario['email']?>">
        
        <div class="custom-select">
            <select class="select" id="select" name="options">
                <option value="admin" <?= ($usuario['role']=='admin')?"selected":""?>>Admin</option>
                <option value="student"<?= ($usuario['role']=='student')?"selected":""?>>Student</option>
            </select>
        </div>
        <button class="button-users" type="submit">Accept changes</button>
    </form>    
    
<?php }else{?>

    <h2 class="h2-subjects">Edit subject</h2>

    <form action="edit-elements.php?id=<?=$subject['id']?>" class="form-subjects" method="post">
        <input class="input-subjects" type="text" name="key-subject" placeholder="Key Subject *" value="<?=$subject['key_subject']?>">
        <input class="input-subjects" type="text" name="subject-name" placeholder="Subject Name *" value="<?=$subject['name']?>">
        <button class="button-subjects" type="submit">Accept Changes</button>
    </form>    
<?php 
    }

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(!empty($_POST["name"]) && !empty($_POST["options"]) && !empty($_POST["email"])){
            
            $name = $_POST["name"];
            $role = $_POST["options"];
            $email = $_POST["email"];

            $sql_update = "UPDATE users SET name = :name, role = :role, email = :email WHERE id = :usuarioId";
            $stmt_update = $conn->prepare($sql_update);
            $stmt_update->bindParam(':name', $name);
            $stmt_update->bindParam(':role', $role);
            $stmt_update->bindParam(':email', $email);
            $stmt_update->bindParam(':usuarioId', $usuario_id);


            $stmt_update->execute();

            echo '<script>window.location.href="edit-elements.php?id='.$usuario_id.'&mensaje=actualizado";</script>';
           
            

        }elseif(!empty($_POST["key-subject"]) && !empty($_POST["subject-name"])){

            $key_subject = $_POST["key-subject"];
            $subject_name = $_POST["subject-name"];

            $sql_update = "UPDATE subjects SET subject_key = :keysubject, subject_name = :name WHERE id = :subjectId";
            $stmt_update = $conn->prepare($sql_update);
            $stmt_update->bindParam(':keysubject', $key_subject);
            $stmt_update->bindParam(':name', $subject_name);
            $stmt_update->bindParam(':subjectId', $subject_id);

            $stmt_update->execute();

            echo '<script>window.location.href="edit-elements.php?id='.$subject_id.'&mensaje=actualizado";</script>';
        
        }else{
            if($_SESSION['rol']=="admin"){
                echo "<p style='font-size: .7em; position:absolute; text-align:center; width:100%; top:95%;'>*No puedes dejar campos vacíos*</p>";
            }else{
                echo "<p style='font-size: .7em; position:absolute; text-align:center; width:100%; top:80%;'>*No puedes dejar campos vacíos*</p>";
            }
        }

    }
    include "../includes/footer.php";
?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>

<?php
    }else{
        echo '<script>window.history.go(-1)</script>';
    exit;
    }
?>
    