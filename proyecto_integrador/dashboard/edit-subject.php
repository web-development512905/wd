<?php
    require "../mysql/functions.php";
    session_start();
    if(!isset($_SESSION["user"]) || $_SESSION['rol'] != "admin"){
        echo '<script>window.history.go(-1)</script>';
        exit;
    }
    validate_user();
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $subject_id = $_GET['id'];
            $sql = "SELECT * FROM subjects WHERE id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $subject_id);
            $stmt->execute();
        
            if($stmt->rowCount() > 0){
                $subject = $stmt->fetch(PDO::FETCH_ASSOC);
            }else{
                echo '<script>window.history.go(-1)</script>';
                exit;
            }
        
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/add-elements.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title><?=$subject['subject_name']?></title>
</head>
<body>

<header>
    <?=include "../includes/header.php";?>
</header>

<?php 

    if(isset($_GET['mensaje']) && $_GET['mensaje'] === "actualizado"){
        echo "<h3 style='text-decoration:underline; font-style:italic;color:#fff; font-size: 1em; position:absolute; text-align:center; width:100%; top:5;'>*It's been edited successfully*</h3>";
    }    
?>   

    <h2 class="h2-subjects">Edit subject</h2>

    <form action="edit-subject.php?id=<?=$subject['id']?>" class="form-subjects" method="post">
        <input class="input-subjects" type="text" name="key-subject" placeholder="Key Subject *" value="<?=$subject['subject_key']?>">
        <input class="input-subjects" type="text" name="subject-name" placeholder="Subject Name *" value="<?=$subject['subject_name']?>">
        <input class="input-subjects" type="text" name="teacher-name" placeholder="Teacher Name *" value="<?=$subject['teacher_name']?>">
        <button class="button-subjects" type="submit">Accept Changes</button>
    </form>    
<?php 

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(!empty($_POST["key-subject"]) && !empty($_POST["subject-name"])&& !empty($_POST["teacher-name"])){
           
            $keySubject = substr($_POST['key-subject'], 0, 5);
            $subject_name = $_POST["subject-name"];
            $teacherName = $_POST["teacher-name"];

            $sql_update = "UPDATE subjects SET subject_key = :keysubject, subject_name = :name, teacher_name = :teacher_name WHERE id = :subjectId";
            $stmt_update = $conn->prepare($sql_update);
            $stmt_update->bindParam(':keysubject', $keySubject);
            $stmt_update->bindParam(':name', $subject_name);
            $stmt_update->bindParam(':teacher_name', $teacherName);
            $stmt_update->bindParam(':subjectId', $subject_id);

            $stmt_update->execute();

            echo '<script>window.location.href="edit-subject.php?id='.$subject_id.'&mensaje=actualizado";</script>';
        
        }else{
                echo "<p style='font-size: .7em; position:absolute; text-align:center; width:100%; top:95%;'>*No puedes dejar campos vacíos*</p>";
        }

    }
    include "../includes/footer.php";
?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>

<?php
    }else{
        echo '<script>window.history.go(-1)</script>';
    exit;
    }
?>
    