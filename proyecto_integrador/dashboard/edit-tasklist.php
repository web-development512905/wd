<?php 
    include "../mysql/functions.php";
    session_start();
    if(!isset($_SESSION["user"]) || $_SESSION['rol'] != "student"){
        echo '<script>window.history.go(-1)</script>';
        exit;
    }
    $task = getTask($conn);
    $subject = lookForSubject($task['id'], $conn);
?>

<!doctype html>
<html lang="en">

<head>
  <title>Task List</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="../assets/css/tasklist.css">
  <!-- Bootstrap CSS v5.2.1 -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

</head>

<body>
  <header>
    <?=include "../includes/header.php"?>
  </header>
  <?=msgEdited()?>
  <main class="container">
    <h1 class="mt-4" style="text-align: center;">Editing your homework :)</h1>

    <div class="card mt-5" >
        <div class="card-body">
            <form method="post" action="edit-tasklist.php?edit=<?=$task['id']?>">
                
                <div class="mb-3">
                <label for="task" class="form-label">Task</label>
                <input type="text" value="<?=$task['task_name']?>" class="form-control" name="task" id="task" aria-describedby="helpId" placeholder="Type the title of your activity *" required>
                </div>

                <div class="mb-3">
                <label for="description" class="form-label">Description</label>
                <textarea class="form-control" name="description" id="description" rows="3" placeholder="Describe your activity"><?=$task['description']?></textarea>
                </div>
                <div class="mb-3 deadline">
                <label for="deadline" class="form-label">Deadline</label>
                <input type="date" value="<?=$task['due_date']?>" class="form-select" name="deadline" id="description" rows="3" placeholder="Introduce la descripción"></textarea>
                </div>

                <div class="custom-select mb-3">
                    <label class="custom-label" for="options">Priority</label>
                    <select class="form-select" aria-label="Default" id="select" name="options">
                        <option value="low" <?=($task['priority'] == "low") ? "selected" : ""?>>Low</option>
                        <option value="medium" <?=($task['priority'] == "medium") ? "selected" : ""?>>Medium</option>
                        <option value="high" <?=($task['priority'] == "high") ? "selected" : ""?>>High</option>
                    </select>
                </div>

                <div class="custom-select-2 mb-3">
                    <label for="options" class="form-label">Subjects</label>
                    <select class="form-select" aria-label="Default" id="select" name="subjects" required>
                        <option value=""  disabled selected>Choose a subject *</option>
                        <?=showSubjectsEdit($_SESSION['iduser'],$conn, $task);?>
                    </select>
                </div>
                <input style="display: block; text-align: center;" name="addtask" id="addtask" class="btn btn-primary w-100 my-5" type="submit" value="Edit task">
                
            </form>
            <?= editTask($task['id'], $conn)?>       
        </div>
    </div>

  </main>
  <?= include "../includes/footer.php"?>
  <!-- Bootstrap JavaScript Libraries -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
  <script src="https://kit.fontawesome.com/eb29c0afa2.js" crossorigin="anonymous"></script>
</body>

</html>