<?php require "../config/validation.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reseting your password</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="assets/fonts/material-icon/css/material-design-iconic-font.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <!-- Main css -->
    <link rel="stylesheet" href="../assets/css/email-validation.css">
</head>
<body>

<section class="vh-100 gradient-custom">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card bg-info text-white" style="border-radius: 1rem;">
          <div class="card-body p-5 text-center">

            <div class="mb-md-3 mt-md-4">
              <h2 class="fw-bold mb-2">Email Validation</h2>
              <p class="text-white-20 mb-3">Please enter your email</p>
              <form method="post">
                  <div data-mdb-input-init class="form-outline form-white mb-3">
                    <input placeholder="Your email here" type="email" id="typeEmailX" name="my-email"class="form-control form-control-lg" />
                  </div>
                  <button data-mdb-button-init data-mdb-ripple-init class="mt-3 btn bg-primary text-white btn-lg px-5" type="submit">Validate</button>
                </div>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>