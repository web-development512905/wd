<?php 
    session_start();
    if(!isset($_SESSION["user"])){
        echo '<script>window.history.go(-1)</script>';
        exit;
    }
require "../mysql/functions.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title>Home</title>
</head>
<body>
        
        
    <?php 
        require "../includes/home.php";
        include "../includes/footer.php"; 
    ?>
        
    <script src="https://kit.fontawesome.com/eb29c0afa2.js" crossorigin="anonymous"></script>
</body>
</html>