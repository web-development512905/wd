<?php 
    include "../mysql/controller.php";
    if(!isset($_SESSION["user"]) || $_SESSION['rol'] != "student"){
        echo '<script>window.history.go(-1)</script>';
        exit;
    }
?>

<!doctype html>
<html lang="en">

<head>
  <title>Task List</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="../assets/css/tasklist.css">
  <!-- Bootstrap CSS v5.2.1 -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

</head>

<body>
  <header>
    <?=include "../includes/header.php"?>
  </header>
  <main class="container">
  <h1 class="text-center my-4 ">Task list</h1>

    <div class="card mt-5" >
        <div style="text-align: center;" class="card-header">
            Hello, <?= $_SESSION['user'];?>
        </div>
        <div class="card-body">

            <form method="post">
                
                <div class="mb-3">
                <label for="task" class="form-label">Task</label>
                <input type="text"
                    class="form-control" name="task" id="task" aria-describedby="helpId" placeholder="Type the title of your activity *" required>
                </div>

                <div class="mb-3">
                <label for="description" class="form-label">Description</label>
                <textarea class="form-control" name="description" id="description" rows="3" placeholder="Describe your activity"></textarea>
                </div>
                <div class="mb-3 deadline">
                <label for="deadline" class="form-label">Deadline</label>
                <input type="date" class="form-select" name="deadline" id="description" rows="3" placeholder="Introduce la descripción"></textarea>
                </div>

                <div class="custom-select mb-3">
                    <label class="custom-label" for="options">Priority</label>
                    <select class="form-select" aria-label="Default" id="select" name="options">
                        <option value="" disabled selected>Choose a level of priority</option>
                        <option value="low">Low</option>
                        <option value="medium">Medium</option>
                        <option value="high">High</option>
                    </select>
                </div>

                <div class="custom-select-2 mb-3">
                    <label for="options" class="form-label">Subjects</label>
                    <select class="form-select" aria-label="Default" id="select" name="subjects" required>
                        <option value=""  disabled selected>Choose a subject *</option>
                        <?=showSubjectsPerUser($_SESSION['iduser'],$conn);?>
                    </select>
                </div>
                <input style="display: block; text-align: center;" name="addtask" id="addtask" class="btn btn-primary w-100 my-5" type="submit" value="Add task">
                
            </form>

            <h4 class="card-title mt-4">Pending...</h4>

              <div class="accordion accordion-flush" id="accordionFlushExample">
                <?php foreach($results as $registro){ ?>
                    <form method="post">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-heading<?= $registro['id']?>">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse<?= $registro['id']?>" aria-expanded="true" aria-controls="flush-collapse<?php echo $registro['id']?>">
                                    <a class="pe-2" href="?id=<?= $registro['id'];?>">
                                        <span class="btn btn-outline-danger ">X</span>
                                    </a>
                                    <a href="edit-tasklist.php?edit=<?= $registro['id'];?>"><i class="fa-regular fa-pen-to-square"></i></a>
                                    <div class="form-check form-switch ps-5">
                                        <input type="hidden" name="filledId" value="<?= $registro['id']; ?> id="<?= $registro['id']?>">
                                        <input class="form-check-input" type="checkbox" role="switch" name="task_check" id="flexSwitchCheckDefault<?= $registro['id']?>" value="<?= $registro['completed']?>" onChange="this.form.submit()" <?= ($registro['completed']==1) ? 'checked' : '' ?>>
                                    </div>
                                    <span class="ps-2"><?php echo $registro["task_name"]?></span>
                                </button>
                            </h2>
                            <div id="flush-collapse<?php echo $registro["id"]?>" class="accordion-collapse collapse description" aria-labelledby="flush-heading<?php echo $registro["id"]?>" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body border border-top-0 border-primary">
                                    <p class="description"><?= $registro["description"]?></p>
                                    <p class="due-to">Due to: <?= ($registro["due_date"] == "0000-00-00") ? "" : $registro["due_date"]?></p>
                                    <p class="priority">Prority: <?= $registro["priority"]?></p>
                                    <p class="created-at">Created at: <?= $registro["created_at"]?></p>
                                    <p class="nameSubject">Subject: <?= lookForSubject($registro["subject_id"], $conn);?></p>
                                </div>
                            </div>
                        </div>
                    </form>
                    <hr>
                <?php   } ?>
              </div>   
              
              
                <h4 class="card-title mt-4">Done</h4>

                <div class="accordion accordion-flush" id="accordionFlushExample">
                <?php foreach($subjects as $subject){ ?>
                    <form method="post">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-heading<?= $subject['id']?>">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse<?= $subject['id']?>" aria-expanded="true" aria-controls="flush-collapse<?php echo $subject['id']?>">
                                    <a class="pe-2" href="?id=<?= $subject['id'];?>">
                                        <span class="btn btn-outline-danger ">X</span>
                                    </a>
                                    <a href="edit-tasklist.php?edit=<?= $subject['id'];?>"><i class="fa-regular fa-pen-to-square"></i></a>
                                    <div class="form-check form-switch ps-5">
                                        <input type="hidden" name="filledId" value="<?= $subject['id']; ?> id="<?= $subject['id']?>">
                                        <input class="form-check-input" type="checkbox" role="switch" name="task_check" id="flexSwitchCheckDefault<?= $subject['id']?>" value="<?= $subject['completed']?>" onChange="this.form.submit()" <?= ($subject['completed']==1) ? 'checked' : '' ?>>
                                    </div>
                                    <span class="ps-2"><?php echo $subject["task_name"]?></span>
                                </button>
                            </h2>
                            <div id="flush-collapse<?php echo $subject["id"]?>" class="accordion-collapse collapse description" aria-labelledby="flush-heading<?php echo $subject["id"]?>" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body border border-top-0 border-primary">
                                    <p class="description"><?= $subject["description"]?></p>
                                    <p class="due-to">Due to: <?= ($subject["due_date"] == "0000-00-00") ? "" : $subject["due_date"]?></p>
                                    <p class="priority">Prority: <?= $subject["priority"]?></p>
                                    <p class="created-at">Created at: <?= $subject["created_at"]?></p>
                                    <p class="nameSubject">Subject: <?= lookForSubject($subject["subject_id"], $conn);?></p>
                                </div>
                            </div>
                        </div>
                    </form>
                    <hr>
                <?php   } ?>
                </div> 


        </div>
    </div>

  </main>
  <?= include "../includes/footer.php"?>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
  <script src="https://kit.fontawesome.com/eb29c0afa2.js" crossorigin="anonymous"></script>
</body>

</html>