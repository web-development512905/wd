<?php 
    session_start();
    if(!isset($_SESSION["user"]) || $_SESSION['rol'] != "admin"){
        echo '<script>window.history.go(-1)</script>';
        exit;
    }
require "../mysql/functions.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/home.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

    <title>Users</title>
</head>
<body>

        <div class="title-container">
            <h2 class="title">Admin</h2>
            <p class="nuser"><?= countTotalUsers($conn) ?> Users</p>
            <div class="dropdown">
                <a class="btn btn-primary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Menu
                </a>
                
                <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="home.php">Home</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="subjects.php">Subjects</a></li>
                <li><a class="dropdown-item" href="users.php">Users</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="add-elements.php">Add User</a></li>
                <li><a class="dropdown-item" href="add-subject.php">Add Subject</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="salir.php">Log out</a></li>
                </ul>
            </div>
        </div>
        <h1 class="text-center my-4 ">Users</h1>

        <form method="get">
            <label for="name">User name:</label>
            <input class="name" type="text" name="buscar">
            <button type="submit" class="btn btn-primary" value="Buscar">Search</button>
        </form>
        <div class="grid-header">
            <div class="title-header">Id</div>
            <div class="title-header">Name</div>
            <div class="title-header">Email</div>
            <div class="title-header">Role</div>
        </div>

        <?php 
           searchUser($conn);
            include "../includes/footer.php";
        ?>
    
    <script src="https://kit.fontawesome.com/eb29c0afa2.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>