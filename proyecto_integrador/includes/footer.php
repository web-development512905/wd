<style>
  
/* poppins-800 - latin */
@font-face {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 800;
    src: url("../assets/fonts/poppins/poppins-v5-latin-800.eot");
    /* IE9 Compat Modes */
    src: local("Poppins ExtraBold"), local("Poppins-ExtraBold"), url("../assets/fonts/poppins/poppins-v5-latin-800.eot?#iefix") format("embedded-opentype"), url("../assets/fonts/poppins/poppins-v5-latin-800.woff2") format("woff2"), url("../assets/fonts/poppins/poppins-v5-latin-800.woff") format("woff"), url("../assets/fonts/poppins/poppins-v5-latin-800.ttf") format("truetype"), url("../assets/fonts/poppins/poppins-v5-latin-800.svg#Poppins") format("svg");
    /* Legacy iOS */ }
/* poppins-500 - latin */
@font-face {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    src: url("../assets/fonts/poppins/poppins-v5-latin-500.eot");
    /* IE9 Compat Modes */
    src: local("Poppins Medium"), local("Poppins-Medium"), url("../assets/fonts/poppins/poppins-v5-latin-500.eot?#iefix") format("embedded-opentype"), url("../assets/fonts/poppins/poppins-v5-latin-500.woff2") format("woff2"), url("../assets/fonts/poppins/poppins-v5-latin-500.woff") format("woff"), url("../assets/fonts/poppins/poppins-v5-latin-500.ttf") format("truetype"), url("../assets/fonts/poppins/poppins-v5-latin-500.svg#Poppins") format("svg");
    /* Legacy iOS */ }
  *{
    font-family: Poppins;
    text-transform: none;
    text-decoration: none !important;
    font-style: normal;
  }
  h6 a:hover{
    color: #cccccc !important;
  }
  h6 a:active{
    color: #e5e5e5 !important;
  }
</style>

<footer style="margin-top: 35%;">
  <!-- Remove the container if you want to extend the Footer to full width. -->
  <div>
    <!-- Footer -->
    <footer class="text-center text-white" style="background-color: #3f51b5">
      <!-- Grid container -->
      <div class="container">
        <!-- Section: Links -->
        <section class="mt-5">
          <!-- Grid row-->
          <div class="row text-center d-flex justify-content-center pt-5">
            <!-- Grid column -->
            <div class="col-md-2">
              <h6 class="text-uppercase font-weight-bold">
                <a href="home.php" class="text-white">HOME</a>
              </h6>
            </div>
            <!-- Grid column -->
            <div class="col-md-2">
              <h6 class="text-uppercase font-weight-bold">
                <a href="salir.php" class="text-white">LOG OUT</a>
              </h6>
            </div>
            <!-- Grid column -->
          </div>
          <!-- Grid row-->
        </section>
        <!-- Section: Links -->

        <hr class="my-5" />
        <!-- Section: Social -->
        <section class="text-center mb-5">
          <a href="" class="text-white me-4">
            <i class="fab fa-facebook-f"></i>
          </a>
          <a href="" class="text-white me-4">
            <i class="fab fa-twitter"></i>
          </a>
          <a href="" class="text-white me-4">
            <i class="fab fa-google"></i>
          </a>
          <a href="" class="text-white me-4">
            <i class="fab fa-instagram"></i>
          </a>
          <a href="" class="text-white me-4">
            <i class="fab fa-linkedin"></i>
          </a>
          <a href="https://gitlab.com/jose.vasquez8829" class="text-white me-4">
            <i class="fab fa-github"></i>
          </a>
        </section>
        <!-- Section: Social -->
      </div>
      <!-- Grid container -->

      <!-- Copyright -->
      <div
          class="text-center p-3"
          style="background-color: rgba(0, 0, 0, 0.2)"
          >
        © 2024 Copyright: <i>Jose Roberto Vasquez Luna</i>
      </div>
      <!-- Copyright -->
    </footer>
    <!-- Footer -->
  </div>
</footer>
<?php return;?>