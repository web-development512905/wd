
<style>

*{
    margin: 0;
    padding: 0;
    border: 0;
}
.title-container{
    background-color: aliceblue;
    width: 100%;
    font-family: Poppins;
    position: relative;
    height: 80px;
    
}
.title, .dropdown{
    display: inline-block;
    top: 50%; 
    transform: translateY(-50%);
    position: absolute;
}
.title{
  left: 3%;
  font-size: 1em;
  font-weight: 500;
}
.dropdown{
    right: 0;
    height: 50%;
    width: 9% !important;
    border-radius: 5px;
    z-index: 1;
}



</style>

<div class="title-container">
    <?php
    if($_SESSION['rol']=="admin"){
        echo '<h2 class="title">Admin</h2>';
        echo '<div class="dropdown">';
        echo '<a class="btn btn-primary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
        echo 'Menu';
        echo '</a>';
        
        echo '<ul class="dropdown-menu">';
        echo '<li><a class="dropdown-item" href="home.php">Home</a></li>';
        echo '<li><hr class="dropdown-divider"></li>';
        echo '<li><a class="dropdown-item" href="subjects.php">Subjects</a></li>';
        echo '<li><a class="dropdown-item" href="users.php">Users</a></li>';
        echo '<li><hr class="dropdown-divider"></li>';
        echo '<li><a class="dropdown-item" href="add-elements.php">Add User</a></li>';
        echo '<li><a class="dropdown-item" href="add-subject.php">Add Subject</a></li>';
        echo '<li><hr class="dropdown-divider"></li>';
        echo '<li><a class="dropdown-item" href="salir.php">Log out</a></li>';
        echo '</ul>';
        echo '</div>';
                
    }else{
        echo '<h2 class="title">Student</h2>';
        echo '<div class="dropdown">';
        echo '<a class="btn btn-primary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">';
        echo 'Menu';
        echo '</a>';

        echo '<ul class="dropdown-menu">';
        echo '<li><a class="dropdown-item" href="home.php">Home</a></li>';
        echo '<li><a class="dropdown-item" href="schedule.php">Schedule</a></li>';
        echo '<li><a class="dropdown-item" href="add-schedule.php">Add Subject to Schedule</a></li>';
        echo '<li><hr class="dropdown-divider"></li>';
        echo '<li><a class="dropdown-item" href="tasklist.php">Task List</a></li>';
        echo '<li><hr class="dropdown-divider"></li>';
        echo '<li><a class="dropdown-item" href="salir.php">Log out</a></li>';
        echo '</ul>';
        echo '</div>';

    }
?>
</div>
<?php return;?>
