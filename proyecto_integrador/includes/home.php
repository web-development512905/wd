<style>
*{
	padding: 0;
	margin: 0;
	border: none;
    font-family: "Poppins";

}
body{
    background-color: hsl(215, 100%, 66%);
}
h1{
	width: 100%;
	margin-top: 2.5%;
	text-align: center;
	color: #ffffff;
    font-weight:700;
}
.next-u{
	background-color: #fff;
	color: #000;
	text-decoration: none;
    display: inline-block;
	width: 40%;
	min-width:250px;
	margin: 15% auto 0 auto;
	text-align:center;
	border-radius: 30px;
    font-weight:800;
	font-size: 4em;
	padding:8% 0;
}
div{
	text-align:center;
	margin-top: 4em; 
	width: 100%;
}
.next{
	background-color: #fff;
	color: #000;
	text-decoration: none;
    display: inline-block;
	width: 40%;
	min-width:250px;
	margin: 10% 2.5% 0 2.5%;
	text-align:center;
	border-radius: 30px;
    font-weight:800;
	font-size: 4em;
	padding:8% 0;

}
.next-u:hover, .next:hover{
    background-color: hsl(215, 100%, 30%);
	color: #fff;
	border-radius: 30px;
}
.next-u:hover, .next:active{
    background-color:hsl(215, 100%, 36%) ;
}
</style>

<h1>Bienvenido, <?= $_SESSION['user']?></h1>

<?php if($_SESSION['rol'] == "admin"){?>
	<div>
		<a class="next" href="users.php">Users</a></button>
		<a class="next" href="subjects.php">Subjects</a></button>
	</div>
<?php }else{ ?>
	<div>
		<a class="next" href="schedule.php">Schedule</a></button>
		<a class="next" href="tasklist.php">Task List</a></button>
	</div>
<?php } ?>
