    <?php
        if(isset($_POST['subject-name'], $_POST['key-subject'],  $_POST['teacher-name'])){
            if(!empty($_POST['subject-name']) && !empty($_POST['key-subject']) && !empty($_POST['teacher-name'])){

            $nameSubject = $_POST['subject-name'];
            $keySubject = substr($_POST['key-subject'], 0, 5);
            $teacherName = $_POST['teacher-name'];

            require '../config/connection.php';

            // Consultar si el correo existe

            $validate_sql = "SELECT count(*) AS count FROM subjects where subject_key = :key_subject";
            $validate_sql = $conn->prepare($validate_sql);
            $validate_sql->bindParam(':key_subject', $keySubject, PDO::PARAM_STR);
            $validate_sql->execute();
            $row = $validate_sql->fetch(PDO::FETCH_ASSOC);


            if ($row['count'] > 0){
                echo "<p class='p-subjects'>*Esta clave de materia ya está registrada*</p>";
            }             
            else{

                 // Insertar en la base de datos

                $query = "INSERT INTO subjects(subject_key, subject_name, teacher_name)
                VALUE(:key_subject, :name, :teacher_name)"; #Aquí le moví -------------

                $resultado = $conn->prepare($query);


                $resultado->bindParam(':key_subject', $keySubject, PDO::PARAM_STR);
                $resultado->bindParam(':name', $nameSubject, PDO::PARAM_STR);
                $resultado->bindParam(':teacher_name', $teacherName, PDO::PARAM_STR);
                $resultado->execute(); // Aquí le moví ---------------

                echo "<p class='p-subjects' style='color:gree;'>*Materia agregada*</p>";
                
            }           

            }else{
                echo "<p class='p-subjects'>*No puedes dejar campos vacíos*</p>";
            }
        }


    ?>