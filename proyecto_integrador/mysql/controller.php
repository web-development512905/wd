<?php
    
    include "functions.php";
    session_start();
    $error = 0;

    if (isset($_SESSION['iduser'])) {
        $userId = $_SESSION['iduser'];
    }

    // Registrar la actividad
    if(isset($_POST['task'],$_POST['subjects'])){
        if(empty($_POST['task']) || empty($_POST['subjects'])){
            $error = 1;
        } else {
            if(empty($_POST['deadline'])){
                $date = "0000-00-00";
            }else{
                $date = $_POST['deadline'];
            }
            if(empty($_POST['options'])){
                $priority = "low";
            }else{
                $priority = $_POST['options'];
            }
            $subject = $_POST['subjects'];
            $task = $_POST['task'];
            $description = $_POST['description'];

            // Insertar la informaciÃ³n a la base de datos
            $sql = "INSERT INTO tasks(task_name, description, due_date, priority, completed, user_id, subject_id) VALUE (:title, :description, :date, :priority, 0, :userid, :subjectId)";
            $sql = $conn->prepare($sql);
            
            $sql->bindParam(':title', $task, PDO::PARAM_STR);
            $sql->bindParam(':description', $description, PDO::PARAM_STR);
            $sql->bindParam(':userid', $userId, PDO::PARAM_INT);
            $sql->bindParam(':priority', $priority, PDO::PARAM_STR);
            $sql->bindParam(':date', $date, PDO::PARAM_STR);
            $sql->bindParam(':subjectId', $subject, PDO::PARAM_INT);
            $sql->execute(); // Aquí le moví ---------------
            
            redirect("/proyecto_integrador/dashboard/tasklist.php");
        }
    }    
    // Cambiar el estatus de la actividad
    if(isset($_POST['filledId'])){
        $filledId = $_POST['filledId'];
        $task_check = (isset($_POST['task_check']))?1:0;
        $sql = "UPDATE tasks SET completed = :task_check WHERE id = :filledId";
        $sql = $conn->prepare($sql);
        $sql->bindParam(':task_check', $task_check, PDO::PARAM_INT);
        $sql->bindParam(':filledId', $filledId, PDO::PARAM_INT);
        $sql->execute();
        
        redirect("/proyecto_integrador/dashboard/tasklist.php");


    }

    // Eliminar actividad
    if(isset($_GET['id'])){
        $task = $_GET['id'];
        // Eliminar la información a la base de datos
        $sql = "DELETE FROM tasks WHERE id = '$task'";
        $sql = $conn->prepare($sql);
        $sql->execute();

        redirect("/proyecto_integrador/dashboard/tasklist.php");


    }

    // Mostrar listado completo de las actividades
    $sql = "SELECT * FROM tasks where user_id = '$userId'and completed = 0  ORDER BY subject_id";
    $results = $conn->query($sql);

    // Mostrar listado completo de las actividades
    $sql = "SELECT * FROM tasks where user_id = '$userId'and completed = 1  ORDER BY subject_id";
    $subjects = $conn->query($sql);

?>