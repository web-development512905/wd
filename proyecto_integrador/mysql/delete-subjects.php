<?php

    session_start();
    if(!isset($_SESSION["user"])){
        echo '<script>window.history.go(-1)</script>';
        exit;
    }

    require "../config/connection.php";
    if(isset($_GET['id']) && !empty($_GET['id'])){
        $subjectId = $_GET['id'];

        if($_SESSION['rol'] == "admin"){
            $sql = "DELETE FROM subjects WHERE id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id',$subjectId);
            $stmt->execute();

        }elseif(($_SESSION['rol'] == "student")){
            $sql = "DELETE FROM users_subjects WHERE subject_id = :subject_id AND user_id = :userId";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':subject_id',$subjectId);
            $stmt->bindParam(':userId',$_SESSION['iduser']);
            $stmt->execute();
        }
        echo '<script>window.location.href="../dashboard/subjects.php?mensaje=borrado";</script>';
    }else{
        echo '<script>window.history.go(-1)</script>';
        exit;
    }


?>