<?php

    session_start();
    if(!isset($_SESSION["user"]) || $_SESSION['rol'] != "admin"){
        echo '<script>window.history.go(-1)</script>';
        exit;
    }

    require "../config/connection.php";
    if(isset($_GET['id']) && !empty($_GET['id'])){
        $usuario_id = $_GET['id'];
        $sql = "DELETE FROM users WHERE id = :id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id',$usuario_id);
        $stmt->execute();
        echo '<script>window.location.href="../dashboard/users.php?mensaje=borrado";</script>';
    }else{
        echo '<script>window.history.go(-1)</script>';
        exit;
    }


?>