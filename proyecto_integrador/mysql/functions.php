<?php 

require "../config/connection.php";

function countTotalUsers($conn) {

    if(isset($_GET['buscar']) && !empty($_GET['buscar'])){
        $termino = $_GET['buscar'];
        $sql = "SELECT COUNT(*) as total_users from users WHERE name LIKE '%$termino%' ORDER BY id";
        $stmt = $conn->query($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

    }else{$sql = "SELECT COUNT(*) AS total_users FROM users";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);}

    
    return $row['total_users'];
}

function countTotalSubjects($conn) {
    if(isset($_GET['buscar']) && !empty($_GET['buscar'])){
        $termino = $_GET['buscar'];
        $sql = "SELECT COUNT(*) as total_subjects from subjects WHERE subject_name LIKE '%$termino%' ORDER BY id";
        $stmt = $conn->query($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

    }else{$sql = "SELECT COUNT(*) AS total_subjects FROM subjects";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);}

    
    return $row['total_subjects'];
}

function record_person($conn){
        $sql = "SELECT * from users where id != 0 ORDER BY id";
        $result = $conn->query($sql);
        if($result->rowCount() > 0){
            foreach($result as $user){

                echo "<div class=\"grid-body\">\n";
                echo "  <div class=\"person id\">" . $user['id'] . "</div>\n";
                echo "  <div class=\"person name\">" . $user['name'] . "</div>\n";
                echo "  <div class=\"person email\">" . $user['email'] . "</div>\n";
                echo "  <div class=\"person role\">" . $user['role'] . "</div>\n";
                echo "  <div class=\"person icons\">\n";
                echo "   <a href='../mysql/eliminar-users.php?id=".$user['id']."'><i class='fa-solid fa-trash'></i></a>\n";
                echo "   <a href='../dashboard/edit-elements.php?id=".$user['id']."'><i class='fa-solid fa-pen-to-square'></i></a>\n"; 
                echo "  </div>\n";
                echo "</div>\n";
            }
        }

}

function record_subject($conn){
    $sql = "SELECT * from subjects where id != 0 ORDER BY id";
    $result = $conn->query($sql);
    if($result->rowCount() > 0){
        foreach($result as $subject){

            echo "<div class=\"grid-body\">\n";
            echo "  <div class=\"person id\">" . $subject['subject_key'] . "</div>\n";
            echo "  <div class=\"person name\">" . $subject['subject_name'] . "</div>\n";
            echo "  <div class=\"person name\">".$subject['teacher_name']."</div>\n";
            echo "  <div class=\"person icons\">\n";
            echo "   <a href='../mysql/delete-subjects.php?id=".$subject['id']."'><i class='fa-solid fa-trash'></i></a>\n";
            echo "   <a href='../dashboard/edit-subject.php?id=".$subject['id']."'><i class='fa-solid fa-pen-to-square'></i></a>\n"; 
            echo "  </div>\n";
            echo "</div>\n";
        }
    }    
}
function record_subject2($conn){
    $sql = "SELECT * from subjects where id != 0 ORDER BY id";
    $result = $conn->query($sql);
    if($result->rowCount() > 0){
        foreach($result as $subject){

            echo "<div class=\"grid-body\">\n";
            echo "  <div class=\"person id\">" . $subject['subject_key'] . "</div>\n";
            echo "  <div class=\"person name\">" . $subject['subject_name'] . "</div>\n";
            echo "  <div class=\"person name\">".$subject['teacher_name']."</div>\n";
            echo "  <div class=\"person icons\">\n";
            echo "   <a>2024-B</a>\n";
            echo "  </div>\n";
            echo "</div>\n";
        }
    }    
}

function record_schedule(){
    $sql = "SELECT * from subjects where id != 0 ORDER BY id";
    $result = $conn->query($sql);
    if($result->rowCount() > 0){
        foreach($result as $subject){

            echo "<div class=\"grid-body\">\n";
            echo "  <div class=\"person id\">" . $subject['subject_key'] . "</div>\n";
            echo "  <div class=\"person name\">" . $subject['subject_name'] . "</div>\n";
            echo "  <div class=\"person name\">".$subject['teacher_name']."</div>\n";
            echo "  <div class=\"person icons\">\n";
            echo "   <a href='../mysql/delete-subjects.php?id=".$subject['id']."'><i class='fa-solid fa-trash'></i></a>\n";
            echo "   <a href='../dashboard/edit-subject.php?id=".$subject['id']."'><i class='fa-solid fa-pen-to-square'></i></a>\n"; 
            echo "  </div>\n";
            echo "</div>\n";
        }
    }    
}

function validate_user(){
    if(!isset($_SESSION['user'])){
        echo '<script>window.history.go(-1)</script>';
        $name = $_SESSION['user'];
        return $name;
        exit;
    }
}

function redirect($page){
    header("Location: $page");
}
function show_tasks($id, $conn){

    $sql = "SELECT * FROM tasks where user_id = '$id' ORDER BY completed, created_at DESC";
    $results = $conn->query($sql);
    return $results;

}


function showSubjectsEdit($userId,$conn, $task){
    $sql = $sql = "SELECT s.*
    FROM users u
    JOIN users_subjects us ON u.id = us.user_id
    JOIN subjects s ON us.subject_id = s.id
    WHERE u.id = '$userId'";
    $results = $conn->query($sql);
    if($results->rowCount() > 0){
        foreach ($results as $subject) {
            echo "<option value='" . $subject['id'] . "'" . ($task['subject_id'] == $subject['id'] ? 'selected' : '') . ">" . $subject['subject_name'] . "</option>\n";
        }        
    }
}


function showSubjectsPerUser($userId, $conn){
    $sql = "SELECT s.*
    FROM users u
    JOIN users_subjects us ON u.id = us.user_id
    JOIN subjects s ON us.subject_id = s.id
    WHERE u.id = '$userId'";
    $results = $conn->query($sql);
    if($results->rowCount() > 0){
        foreach($results as $subject){
            echo "<option value='".$subject['id']."'>" . $subject['subject_name'] ."</option>\n";
        }
    }
}


function showSubjects($conn){
    $sql = "SELECT * FROM subjects";
    $results = $conn->query($sql);
    if($results->rowCount() > 0){
        foreach($results as $subject){
            echo "<option value='".$subject['id']."'>" . $subject['subject_name'] ."</option>\n";
        }
    }
}
function lookForSubject($subjectId, $conn){
    $sql = "SELECT * FROM subjects where id = '$subjectId'";
    $resultado = $conn->query($sql);
    if($resultado->rowCount() > 0 ){
        $registro = $resultado->fetch(PDO::FETCH_ASSOC);
        return $registro['subject_name'];
    }
}

function getTask($conn){
    if(isset($_GET['edit']) && !empty($_GET['edit'])){
        $taskId = $_GET['edit'];
        $sql = "SELECT * FROM tasks WHERE id = :id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $taskId);
        $stmt->execute();
    
        if($stmt->rowCount() > 0){
            $task = $stmt->fetch(PDO::FETCH_ASSOC);
        }else{
            echo '<script>window.history.go(-1)</script>';
            exit;
        }
        return $task;
    }
}

function editTask($taskId, $conn){
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(!empty($_POST["task"]) && !empty($_POST["subjects"])){

            if($_POST["deadline"]==""){
                $deadline = "0000-00-00";
            }else{
                $deadline = $_POST["deadline"];
            }
            $title = $_POST["task"];
            $description = $_POST["description"];
            $priority = $_POST["options"];
            $subjectId = $_POST["subjects"];


            $sql_update = "UPDATE tasks SET task_name = :title, description = :description, due_date = :due_date, priority = :priority, subject_id = :subject_id WHERE id = :taskId";
            $stmt_update = $conn->prepare($sql_update);
            $stmt_update->bindParam(':title', $title);
            $stmt_update->bindParam(':description', $description);
            $stmt_update->bindParam(':due_date', $deadline);
            $stmt_update->bindParam(':priority', $priority);
            $stmt_update->bindParam(':subject_id', $subjectId);
            $stmt_update->bindParam(':taskId', $taskId);

            $stmt_update->execute();

            echo '<script>window.location.href="edit-tasklist.php?edit='.$taskId.'&mensaje=actualizado";</script>';
        }
    }
}
function addSubject($userId, $conn){
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(!empty($_POST["key-subject"])){
            $key = $_POST["key-subject"];
            $sql = "SELECT id FROM subjects WHERE subject_key = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $key);
            $stmt->execute();
            if($stmt->rowCount() > 0){
                $subject = $stmt->fetch(PDO::FETCH_ASSOC);
                $sql = "INSERT INTO users_subjects (user_id, subject_id) VALUE (:userId, :subjectId)";
                $result = $conn->prepare($sql);
                $result->bindParam(':userId', $userId, PDO::PARAM_INT);
                $result->bindParam(':subjectId', $subject['id'], PDO::PARAM_STR);
                $result->execute();
                redirect("schedule.php");
            }         
        }
    }
}

function subjectPerUser($userId,$conn){
    $sql = "SELECT s.*
    FROM users u
    JOIN users_subjects us ON u.id = us.user_id
    JOIN subjects s ON us.subject_id = s.id
    WHERE u.id = '$userId'";

    $result = $conn->query($sql);
    if($result->rowCount() > 0){
        foreach($result as $subject){
            echo "<div class=\"grid-body\">\n";
            echo "  <div class=\"person id\">" . $subject['subject_key'] . "</div>\n";
            echo "  <div class=\"person name\">" . $subject['subject_name'] . "</div>\n";
            echo "  <div class=\"person name\">".$subject['teacher_name']."</div>\n";
            echo "  <div class=\"person icons\">\n";
            echo "   <a href='../mysql/delete-subjects.php?id=".$subject['id']."'><i class='fa-solid fa-trash'></i></a>\n";
            echo "  </div>\n";
            echo "</div>\n";
        }
    }
}
function searchUser($conn){
    if(isset($_GET['buscar']) && !empty($_GET['buscar'])){
        $termino = $_GET['buscar'];
        $sql = "SELECT * from users WHERE name LIKE '%$termino%' ORDER BY id";
        $result = $conn->query($sql);
        if($result->rowCount() > 0){
            foreach($result as $user){
    
                echo "<div class=\"grid-body\">\n";
                echo "  <div class=\"person id\">" . $user['id'] . "</div>\n";
                echo "  <div class=\"person name\">" . $user['name'] . "</div>\n";
                echo "  <div class=\"person email\">" . $user['email'] . "</div>\n";
                echo "  <div class=\"person role\">" . $user['role'] . "</div>\n";
                echo "  <div class=\"person icons\">\n";
                echo "   <a href='../mysql/eliminar-users.php?id=".$user['id']."'><i class='fa-solid fa-trash'></i></a>\n";
                echo "   <a href='../dashboard/edit-elements.php?id=".$user['id']."'><i class='fa-solid fa-pen-to-square'></i></a>\n"; 
                echo "  </div>\n";
                echo "</div>\n";
            }
        }
    }else{
        record_person($conn);
        $termino = "";
    }
}
function searchSubject($conn){
    if(isset($_GET['buscar']) && !empty($_GET['buscar'])){
        $termino = $_GET['buscar'];
        $sql = "SELECT * from subjects WHERE subject_name LIKE '%$termino%' ORDER BY id";
        $result = $conn->query($sql);
        if($result->rowCount() > 0){
            foreach($result as $subject){
    
                echo "<div class=\"grid-body\">\n";
                echo "  <div class=\"person id\">" . $subject['subject_key'] . "</div>\n";
                echo "  <div class=\"person name\">" . $subject['subject_name'] . "</div>\n";
                echo "  <div class=\"person name\">".$subject['teacher_name']."</div>\n";
                echo "  <div class=\"person icons\">\n";
                echo "   <a href='../mysql/delete-subjects.php?id=".$subject['id']."'><i class='fa-solid fa-trash'></i></a>\n";
                echo "   <a href='../dashboard/edit-subject.php?id=".$subject['id']."'><i class='fa-solid fa-pen-to-square'></i></a>\n"; 
                echo "  </div>\n";
                echo "</div>\n";
            }
        }
    }else{
        record_subject($conn);
        $termino = "";
    }


}
function msgEdited(){
    if(isset($_GET['mensaje']) && $_GET['mensaje'] === "actualizado"){
        echo "<h3 style='text-decoration:underline; font-style:italic;color:#fff; font-size: 1em; position:absolute; text-align:center; width:100%; top:5;'>*It's been edited successfully*</h3>";
    }  
}

function getSubjectId($conn){
    if(isset($_POST['key-subject']) && !empty($_POST['key-subject'])){
        $subjectKey = $_POST['key-subject'];
        $sql = "SELECT * FROM subjects WHERE subject_key = :key";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':key', $subjectKey);
        $stmt->execute();
    
        if($stmt->rowCount() >= 1){
            $subject = $stmt->fetch(PDO::FETCH_ASSOC);
            return $subject['id'];
        }
    }
}
function isInSchedule($conn, $userID){
        $subjectKey = getSubjectId($conn);
        $sql = "SELECT * FROM users_subjects WHERE subject_id = :key and user_id = :user";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':key', $subjectKey);
        $stmt->bindParam(':user', $userID);
        $stmt->execute();
    
        if($stmt->rowCount() >= 1){
            return 1;
        }
}
?>